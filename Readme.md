# Телеграм бот

## Установка
Для установки и запуска бота необходимо иметь на системе python v3.10^ и redis

```bash
pip3 install python-telegram-bot==13.15 requests redis kafka-python
git@gitlab.com:icstech/telegram-bot.git
cd telegram-bot/project
cp TelegramService/ENV.example.py TelegramService/ENV.py # редактировать под свои значения ENV.py
python3 main.py
```

Есть возможность создания отдельного интерпретатора, чтобы не использовать главный
в специальную папку (venv). Тут будет питон не засоренный другими, ненужными библиотеками и не затронутый изменениями, которые
могут возникнуть, например при `apt upgrade`
Если хотите создать виртуальное окружение:
```bash
sudo apt update
sudo apt install software-properties-common -y
sudo add-apt-repository ppa:deadsnakes/ppa
sudo apt install python3.10 python3.10-venv
sudo apt install redis
cd telegram-bot/project
cp TelegramService/ENV.example.py TelegramService/ENV.py # редактировать под свои значения ENV.py
python3.10 -m venv ./venv
source venv/bin/activate

pip intall python-telegram-bot==13.15 requests redis kafka-python
python main.py

#########################
# Отлкючение окружения 
deactivate

# Последующие запуски 
cd telegram-bot/project
source venv/bin/activate
python main.py
```


## Описание работы

Структура проекта состоит из:  
файла `main.py` - основная точка входа  
папки `TelegramService`, котора включает в себя:  

* папка `Command`:   
В папку добавляются файлы с функциями, которые выполняются при подаче боту в виде '/command'  
`CommandPointer.py` хранит в себе словарь маршрутизации, где ключ - текстовая команда боту после слеша, а значение - 
функция, которую необходимо выполнить  
Например:
```python
from Command.start import *
commands = {
    'start': start
}
#  Значит что при написании боту 
#  `/start` будет выполнятся функция `start()` 
#  из файла `Command/start.py`
```
* папка `InlineButton`:  
Включает в себя функции обработки нажатия "Инлайновых кнопок", handler.py файл является маршрутизатором на функции 
по пришедшим значениям с кнопок 
Значения кнопок __хранят необходимую информацию__ для выполнения функций, на что опирается маршрутизатор

* папка `Message`:  
включает в себя `handler.py`, который принимает текстовые сообщения от пользователя

#### То есть handler для папок Message и InlineButton - это точка входа в программу после получения ботом сообщения

* папка `Service` включает:  

## Полезная информация

* `telegram.Update` класс - это класс с информацией о полученном сообщении, подаётся первым аргументом функции  
Имеет информацию о пользователе и его сообщении
`user=update.effective_user`
* `update.callback_query` - это информация о нажатой InlineButton, хранит в себе информацию о значении кнопки
и информацию о сообщении

### Логгирование

Для логирования нужно использовать встроенный пакет logging  
Пример записи логов:
```python
import logging

### ...
logging.error("Ошибка сервера с запросом " + elsa_response.request.url)
### ...
```

### Создание бота

В телеграме необходимо написать главному боту   
https://t.me/BotFather
```
/newbot
# Написать имя бота
ICS_BOT_MALOY
# Написать уникальный никнейм бота с подписью _bot или Bot в конце имени
ics_maloy_bot

В ответ получаешь все данные по боту, им можно пользоваться
```

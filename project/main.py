import os.path
import logging

from TelegramService.Service.Kafka import Kafka

if not os.path.exists('TelegramService/ENV.py'):
    raise FileNotFoundError('Отсутствует ENV.py файл')

from TelegramService.ENV import TELEGRAM_BOT_TOKEN

from telegram.ext import Updater
from TelegramService.BotSetup import BotSetup
import threading

updater = Updater(token=TELEGRAM_BOT_TOKEN, use_context=True)
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)

bot_setup = BotSetup(updater.dispatcher)
bot_setup.add_shedule()
bot_setup.add_commands()
# bot_setup.add_message_handler()
# bot_setup.add_button_handler()


#
# kafka_consumer = Kafka()
# kafka_thread = threading.Thread(target=kafka_consumer.getMessagePool)
# kafka_thread.start()

updater.start_polling()
updater.idle()


from telegram.ext import CallbackContext, Updater
import TelegramService.AdminCommand.Action as Action
from TelegramService.ENV import TELEGRAM_BOT_TOKEN

from TelegramService.AdminCommand.functions.test_action import test_action


def echo(action, data):
    context = CallbackContext(Updater(token=TELEGRAM_BOT_TOKEN, use_context=True).dispatcher)
    if action == Action.TEST_ACTION:
        test_action(context, data)
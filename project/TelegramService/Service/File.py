import requests

from TelegramService.ENV import TELEGRAM_BOT_TOKEN


class File:
    def __init__(self, attachment):
        self.__attachment = None
        self.have_content = False
        self.text = None
        self.have_text = False
        if attachment.effective_attachment is not None:
            if isinstance(attachment.effective_attachment, list):
                self.__attachment = attachment.effective_attachment
            else:
                self.__attachment = [attachment.effective_attachment]
            self.have_content = True
            if attachment.caption:
                self.text = attachment.caption
                self.have_text = True

    def get_url(self):
        file_id = self.__attachment[-1].file_id
        response = requests.get(url=f"https://api.telegram.org/bot{TELEGRAM_BOT_TOKEN}/getFile?file_id={file_id}")
        if response.status_code == 200:
            file_path = response.json()['result']['file_path']
            return f"https://api.telegram.org/file/bot{TELEGRAM_BOT_TOKEN}/" + file_path



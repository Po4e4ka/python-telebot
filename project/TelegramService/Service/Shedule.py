import datetime
from telegram.ext import CallbackContext
import TelegramService.Message.CHAT as CHAT

class SheduleType:
    daily = 'd'

class Shedule:

    def __init__(self, jobDispatcher):
        self.jobDispatcher = jobDispatcher
        self.types = SheduleType

    def add(self, command, sheduleType, at=0):
        if sheduleType == SheduleType.daily:
            now = datetime.datetime.now()
            if now.hour < at:
                begin = datetime.datetime(now.year, now.month, now.day, at)
            else:
                begin = datetime.datetime(now.year, now.month, now.day+1, at)
            diffSeconds = int((begin - now).total_seconds())
            self.jobDispatcher.run_repeating(command, interval=60*60*24, first=diffSeconds)

    @staticmethod
    def sendDaysCountToMyPantera(context: CallbackContext = None):
        daysCount = int(format(datetime.datetime.fromisoformat('2023-03-08'), '%j')) - \
                    int(format(datetime.datetime.now(), '%j'))
        context.bot.sendMessage(CHAT.MASHA,
                                'Доброе утро! '
                                f'До встречи с  Лёшечкой осталось '
                                f'{daysCount} дней')


if __name__ == '__main__':
    pass
import re
from typing import Iterable

from telegram import InlineKeyboardButton, InlineKeyboardMarkup

from TelegramService.InlineButton.Button import MAIN_MENU


class InlineKeyboard:
    def __init__(self):
        self.__keyboard = []

    def get(self):
        return InlineKeyboardMarkup(self.__keyboard)

    def add_button(self, button_name: str, button_value: str, url: str = None):
        """
        :param button: (Название кнопки, значение кнопки)
        """
        self.__keyboard.append([InlineKeyboardButton(text=button_name, callback_data=button_value, url=url)])


    def add_button_from_array(self, button_array: Iterable):
        for button in button_array:
            button_len = len(button)
            if button_len == 2:
                self.add_button(button[0], button[1])
            elif button_len == 3:
                self.add_button(button[0], button[1], button[2])
            else:
                raise Exception("Неправильный формат кнопки при создании массива кнопок")

    def last_button(self) -> InlineKeyboardButton:
        return self.__keyboard[-1]

    def add_main_menu_button(self, text='Главное меню'):
        self.add_button(text, MAIN_MENU)


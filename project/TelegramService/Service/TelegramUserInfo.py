import json
import logging

import redis

from TelegramService.ENV import REDIS_HOST, REDIS_PORT
from TelegramService.Service.InlineKeyboard import InlineKeyboard


class TelegramUserInfo:

    def __init__(self, user):
        self.__info = {}
        self.__user = user
        self.__redis = redis.Redis(REDIS_HOST, REDIS_PORT)
        if self.info is None:
            self.clear(True)

    def __add__(self, other: dict):
        return self.info | other

    def __sub__(self, other: str):
        self.info.pop('lol')
        self.__dump()
        return self

    def __str__(self):
        return str(self.info)

    def __repr__(self):
        return self.info

    def __or__(self, other: dict):
        return self.info | other

    def __ror__(self, other: dict):
        return self.__or__(other)

    def __getitem__(self, item):
        return self.get(item)

    def __setitem__(self, item, value):
        self.info[item] = value
        self.__dump()
        return self.get(item)

    @property
    def info(self):
        self.__load()
        return self.__info

    @info.setter
    def info(self, value: dict):
        self.__info = value
        self.__dump()

    @property
    def method(self):
        return self['method']

    @method.setter
    def method(self, value: dict):
        self['method'] = value

    @property
    def delete_messages(self):
        delete_message = self['delete_messages']
        return [] if delete_message is None else delete_message

    @delete_messages.setter
    def delete_messages(self, value: dict):
        self['delete_messages'] = value

    def __load(self):
        temp = self.__redis.get(self.__user)
        self.__info = temp if temp is None else json.loads(temp)

    def __dump(self):
        self.__redis.set(self.__user, json.dumps(self.__info))

    def clear(self, first=False):
        if first:
            self.info = {'chat_id': None}
        else:
            temp = self.info['chat_id']
            self.info = {'chat_id': temp if temp else None}

    def add(self, key, value=True):
        self.info = self.__add__({key: value})

    def pop(self, key):
        temp = self.info.pop(key)
        self.__dump()
        return temp

    def get(self, key):
        return self.__info[key] if key in self.info else None

    def add_delete_message(self, delete_message_id: int):
        temp = self.delete_messages
        if temp:
            temp.append(delete_message_id)
        else:
            temp = [delete_message_id]
        self.delete_messages = temp

    def delete_from_array(self, chat_id, context):
        delete_messages = self.delete_messages
        delete_messages = [] if delete_messages is None else delete_messages
        for message_id in delete_messages:
            if message_id == -1:
                pass
            else:
                try:
                    context.bot.delete_message(chat_id, message_id)
                except Exception as e:
                    logging.warning("Попытка удалить сообщение, которого нет")
        self.delete_messages = []

    def delete(self):
        self.info = {}


class SelectedApartment():
    def __init__(self):
        self.apartment_id = ''
        self.full_address = ''
        self.account_number = ''

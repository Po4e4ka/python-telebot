import json
import time
from logging import getLogger

import kafka

from TelegramService.AdminCommand.handler import echo
from TelegramService.ENV import KAFKA_HOST, KAFKA_PORT, KAFKA_USERNAME, KAFKA_PASSWORD

import ssl

# sasl_mechanism = 'PLAIN'
# security_protocol = 'SASL_SSL'
#
# # Create a new context using system defaults, disable all but TLS1.2
# context = ssl.create_default_context()
# context.options &= ssl.OP_NO_TLSv1
# context.options &= ssl.OP_NO_TLSv1_1
class Kafka:
    def __init__(self):
        while True:
            try:
                self.kafka_consumer = kafka.KafkaConsumer(
                    'telegram_bot_admin_command',
                    bootstrap_servers=KAFKA_HOST + ':' + KAFKA_PORT,
                    group_id='telegram_consumer',
                    # sasl_plain_username=KAFKA_USERNAME,
                    # sasl_plain_password=KAFKA_PASSWORD,
                    # security_protocol=security_protocol,
                    # ssl_context=context,
                    # sasl_mechanism=sasl_mechanism,
                )
                break
            except Exception as e:
                getLogger().warning("Kafka connect fail")

                for i in range(10, 0, -1):
                    getLogger().warning(f"Retry by {i}")
                    time.sleep(1)

    def getMessagePool(self):
        if not self.kafka_consumer.bootstrap_connected():
            raise ConnectionError('Ошибка подключения к топику')
        while True:
            topic_dict = self.kafka_consumer.poll()
            if topic_dict != {}:
                for message_array in topic_dict:
                    for message in topic_dict[message_array]:
                        echo(message.key.decode(), json.loads(message.value.decode()))
            time.sleep(1)


if __name__ == '__main__':
    kafka = Kafka()
    kafka.getMessagePool()

from telegram.ext import CommandHandler, MessageHandler, Filters, CallbackQueryHandler, Updater

from TelegramService.Command import CommandPointer
import TelegramService.Message.handler as message_handler
import TelegramService.InlineButton.handler as button_handler
import TelegramService.AdminCommand.handler as admin_handler
from TelegramService.Service.Shedule import Shedule


class BotSetup:
    def __init__(self, dispatcher):
        self.dispatcher = dispatcher

    def add_shedule(self):
        j = self.dispatcher.job_queue
        shedule = Shedule(j)
        shedule.add(Shedule.sendDaysCountToMyPantera, shedule.types.daily, 10)

    def add_commands(self):
        for name, command in CommandPointer.commands.items():
            self.dispatcher.add_handler(CommandHandler(name, command))

    def add_message_handler(self):
        admin_echo_handler = MessageHandler(
            Filters.chat(-850663741) & Filters.text & (~Filters.command),
            admin_handler.echo)
        echo_handler = MessageHandler((Filters.attachment | Filters.text) & (~Filters.chat(-850663741) &
                                      (~Filters.command)),
                                      message_handler.echo)

        self.dispatcher.add_handler(echo_handler)
        self.dispatcher.add_handler(admin_echo_handler)

    def add_button_handler(self):
        self.dispatcher.add_handler(CallbackQueryHandler(button_handler.echo))

from TelegramService.InlineButton import Button
from telegram import Update
from telegram.ext import CallbackContext

from TelegramService.Service.InlineKeyboard import InlineKeyboard
from TelegramService.Service.TelegramUserInfo import TelegramUserInfo


def start(update: Update, context: CallbackContext):
    telegram_user = update.effective_user
    database = TelegramUserInfo(telegram_user.id)
    database['chat_id'] = update.effective_chat.id
    show_main_menu_buttons(update, context)


def show_main_menu_buttons(update, context, delete=True):
    keyboard = InlineKeyboard()
    database = TelegramUserInfo(update.effective_user.id)
    if delete:
        database.delete_from_array(update.effective_chat.id, context)
    database.method = Button.MAIN_MENU
    keyboard.add_button_from_array([
        ("Кнопка из главного меню", Button.TEST),
    ])
    message = context.bot.send_message(chat_id=update.effective_chat.id, text='Выберите действие из списка ниже:',
                                       reply_markup=keyboard.get())
    database.add_delete_message(message.message_id)
    database.add_delete_message(update.effective_message.message_id)

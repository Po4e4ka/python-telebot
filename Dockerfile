FROM python:3.10

RUN pip3 install python-telegram-bot requests redis kafka-python; \
    mkdir -p /home/telegram-bot

WORKDIR /home/telegram-bot

COPY ./init-script /usr/bin/init-bot.sh

RUN chmod +x /usr/bin/init-bot.sh

COPY ./project /home/telegram-bot

ENTRYPOINT [ "init-bot.sh" ]
